import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class LayoutService {

  constructor(private http: HttpClient) { }

  obtenerPersonajes(){
    const url = environment.domain_url + '/character';

    return this.http.get<any>(url).pipe(
      retry(2)
    );
  }

  obtenerPersonajePorId(id: number){
    const url = environment.domain_url + '/character/'+id;

    return this.http.get<any>(url).pipe(
      retry(2)
    );
  }

}
