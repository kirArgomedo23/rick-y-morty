export interface Schema {
    id: Number;
    name: string;
    status: string;
    species: string;
    type: string;
    gender: string;
    image: string;
    url: string;
    created: string;
}
