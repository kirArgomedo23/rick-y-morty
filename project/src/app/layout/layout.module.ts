import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';

import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import {PipesModule} from '../shared/pipes.module';
@NgModule({
  declarations: [LayoutComponent],
  imports: [
    CommonModule,ReactiveFormsModule,
    LayoutRoutingModule, FormsModule,
    PipesModule
  ]
})
export class LayoutModule { }
