import { Component, OnInit } from '@angular/core';
import {Schema} from './schema';
import {LayoutService} from './layout.service';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  FILTRO = [{"name":"Alive"}, {"name":"Dead"}, {"name":"unknow"}, {"name":"Todos"} ];
  esquema: Schema []= [];
  FILTRO_APLICADO: string;


  constructor(
    private layoutService: LayoutService
  ) { }

  ngOnInit(): void {
    this.layoutService.obtenerPersonajes().subscribe((data) => {
      this.esquema = data['results']
    });
  }

  obtenerFiltro(e){
    this.FILTRO_APLICADO = e.toString();
  }

  buscarPersonaje(buscar: string): Schema []{
    if (buscar === "") return this.esquema;
    let datos:Schema [] = [];
    this.esquema.map(data => {
      if(data.name.toLowerCase().includes(buscar.toLowerCase()))  datos.push(data);
    })
    
    return this.esquema = datos;
  }

}
