import { Pipe, PipeTransform } from '@angular/core';
import {Schema} from '../layout/schema';

@Pipe({
  name: 'filtrar'
})
export class FiltrarPipe implements PipeTransform {

  esquema: Schema [] = [];

  transform(value: Schema [], filtro: string): Schema [] | string {
    if(filtro == undefined || filtro === 'Todos') return value;

    this.esquema = [];
    value.map(esq => {
      if(esq.status === filtro){
        this.esquema.push(esq);
      } 
    });

    return this.esquema;
  }

}
